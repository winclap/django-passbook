Django-passbook
===============

[ ![Codeship Status for winclap/django-passbook](https://codeship.com/projects/f50a3420-e604-0132-c757-46daeabcd7f9/status?branch=master)](https://codeship.com/projects/82170)

About
-----

Django-passbook is a simple Django app for manage ios passbook's webservices.

Getting Started
---------------

    $ pip install git+https://meiyo@bitbucket.org/winclap/django-passbook.git

Configure Settings
------------------

1.  Add "passbook" to your INSTALLED_APPS setting like this:

        INSTALLED_APPS = (
            ...
            # The Django Content Types framework is required
            'django.contrib.contenttypes',
            ...
            'passbook',
            ...
        )

2.  Configure a certificates and keys in your settings.py:

        PASSBOOK_KEY = '...'
        PASSBOOK_CERTIFICATE = '...'
        PASSBOOK_WWWDR_CERTIFICATE = '...'
        PASSBOOK_PASSPHRASE = '...'

3.  Include the passbook URLconf in your project urls.py like this:

        urlpatterns = [
            url(r'^passbook/', include('passbook.urls')),
        ]

4.  Run `python manage.py migrate` to create the passbook models.

    > # For Django < 1.7
    >
    > django-passbook suport South migrations, add the migrations
    > modules to your settings.py like this:
    >
    >         SOUTH_MIGRATION_MODULES = {
    >             'passbook': 'passbook.south_migrations',
    >     }

Usage
-----

### Models

```python
from passbook.models import PkPass, PkPassField, PkPassBarcode


barcode = PkPassBarcode.objects.create(
    barcode_format=PkPassBarcode.PDF417,
    message='123456789',
    message_encoding='iso-8859-1'
)

pkpass = PkPass.objects.create(
    pass_type_identifier='pass.com.pawplanet.coupon',
    serial_number='E5982H-I2',
    team_identifier='A1B2C3D4E5',
    web_service_url='https://example.com/passes/',
    authentication_token='vxwxd7J8AlNNFPS8k0a0FfUFtq0ewzFdc',
    barcode=barcode,
    organization_name='Paw Planet',
    logo_text='Paw Planet',
    description='20% off premium dog food',
    foreground_color='rgb(255, 255, 255)',
    background_color='rgb(206, 140, 53)',
    pass_type=PkPass.COUPON_TYPE,
    logo_image=logo_image,
    logo_image_2x=logo_image_2x,
    icon_image=icon_image,
    icon_image_2x=icon_image_2x,
)

PkPassField.objects.create(
    field_type=PkPassField.TYPE_PRIMARY,
    key='offer',
    label='Any premium dog food',
    value='20% off',
    pkpass=pkpass
)

PkPassField.objects.create(
    field_type=PkPassField.TYPE_AUXILIARY,
    key='expires',
    label='EXPIRES',
    value='2 weeks',
    pkpass=pkpass
)

PkPassField.objects.create(
    field_type=PkPassField.TYPE_BACK,
    key='terms',
    label='TERMS AND CONDITIONS',
    value=(
      'Generico offers this pass, including all information, software, '
      'products and services available from this pass or offered as part of '
      'or in conjunction with this pass (the \"pass\"), to you, the user, '
      'conditioned upon your acceptance of all of the terms, conditions, '
      'policies and notices stated here. Generico reserves the right to make '
      'changes to these Terms and Conditions immediately by posting the '
      'changed Terms and Conditions in this location.\n\nUse the pass at '
      'your own risk. This pass is provided to you \"as is,\" without '
      'warranty of any kind either express or implied. Neither Generico nor '
      'its employees, agents, third-party information providers, merchants, '
      'licensors or the like warrant that the pass or its operation will be '
      'accurate, reliable, uninterrupted or error-free. No agent or '
      'representative has the authority to create any warranty regarding the '
      'pass on behalf of Generico. Generico reserves the right to change or '
      'discontinue at any time any aspect or feature of the pass.'),
    pkpass=pkpass
)
```

### Signals

```python
from passbook.signals import pkpass_registered, pkpass_unregistered
from django.dispatch import receiver

@receiver(pkpass_registered)
def pkpass_registered_callback(sender, **kwargs):
    ...

@receiver(pkpass_unregistered)
def pkpass_unregistered_callback(sender, **kwargs):
    ...
```

Contact
-------
* <mailto:developers@winclap.com>
