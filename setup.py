# -*- coding: utf-8 -*-
import sys
from setuptools import setup


with open('requirements/production.txt', 'r') as fh:
    dependencies = [l.strip() for l in fh]

extras = {}

with open('requirements/develop.txt', 'r') as fh:
    extras['tests'] = [l.strip() for l in fh][1:]

with open('requirements/test.txt', 'r') as fh:
    tests_dependencies = [l.strip() for l in fh][1:]

with open('README.md', 'r') as readme:
    README = readme.read()

########### platform specific stuff #############

if sys.version_info[0] == 2 and sys.version_info[1] < 6:
    raise Exception('Python 2 version < 2.6 is not supported')
elif sys.version_info[0] == 3 and sys.version_info[1] < 3:
    raise Exception('Python 3 version < 3.3 is not supported')

#################################################

setup(
    name='django-passbook',
    version='0.1.9',
    packages=['passbook'],
    include_package_data=True,
    license='MIT',
    description='A simple Django app for manage ios passbook\'s webservices.',
    long_description=README,
    url='https://bitbucket.org/winclap/django-passbook',
    author='Federico and others',
    author_email='federico@winclap.com',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    install_requires=dependencies,
    extras_require=extras,
    tests_require=tests_dependencies,
)
