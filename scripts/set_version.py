# -*- coding: utf-8 -*-
"""Script to set the version number wherever it's needed before a release."""
from __future__ import unicode_literals, print_function

import sys
import os
import re
import io


def replace(pattern, repl, path):
    """Replace pattern with repl in a file."""
    with io.open(path, 'r', encoding='utf8') as inf:
        data = inf.read()
        data = re.sub(pattern, repl, data)

    with io.open(path, 'w+', encoding='utf8') as outf:
        outf.write(data)


if __name__ == "__main__":
    INPF = raw_input if sys.version_info[0] == 2 else input
    VERSION = INPF("New version number (in format X.Y.Z): ").strip()
    replace("version='.+'", "version='{0}'".format(VERSION), 'setup.py')
    replace('__version__ = ".*"', '__version__ = "{0}"'.format(VERSION),
            os.path.join('passbook', '__init__.py'))
