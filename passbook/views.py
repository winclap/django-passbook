# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import dateutil.parser

from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.utils import timezone

from restless.views import Endpoint
from restless.http import Http200, Http400, Http201, JSONResponse

from .forms import PkPassLogForm, PkPassRegistrationForm
from .models import PkPassLog, PkPassRegistration, PkPass, Device
from .auth import has_authorization
from .signals import pkpass_registered, pkpass_unregistered


class PkPassRegisterDeleteEndpoint(Endpoint):
    """
    Endpoint for register (POST) and unregister (DELETE) PkPass on a device.
    """

    @has_authorization
    def post(self, request, *args, **kwargs):
        """
        POST request to v1/devices/{device_library_identifier}/registrations/
                                        {pass_type_identifier}/{serial_number}

        Parameters
            device_library_identifier
                A unique identifier that is used to identify and authenticate
                this device in future requests.

            pass_type_identifier
                The pass’s type, as specified in the pass.

            serial_number
                The pass’s serial number, as specified in the pass.

        Header
            The Authorization header is supplied; its value is the word
            “ApplePass”, followed by a space, followed by the pass’s
            authorization token as specified in the pass.

        Payload
            The POST payload is a JSON dictionary, containing a single
            key and value:
                pushToken
                    The push token that the server can use to send push
                    notifications to this device.

        Response
            If the serial number is already registered for this device,
            return HTTP status 200.

            If registration succeeds, return HTTP status 201.

            If the request is not authorized, return HTTP status 401.

            Otherwise, return the appropriate standard HTTP status.

        Discussion
            Any time the pass is updated, your server sends a push notification
            with an empty JSON dictionary as the payload to the device
            using the given push notification token. This continues until
            the device is explicitly unregistered (as described in
            Unregistering a Device).
        """
        registration_form = PkPassRegistrationForm(request.data)
        if registration_form.is_valid():
            pkpass = get_object_or_404(
                PkPass,
                pass_type_identifier=self.kwargs['pass_type_identifier'],
                serial_number=self.kwargs['serial_number']
            )
            device, created = Device.objects.get_or_create(
                device_library_identifier=self.kwargs[
                    'device_library_identifier'
                ]
            )
            registration, created = PkPassRegistration.objects.get_or_create(
                device=device,
                pkpass=pkpass,
                defaults={'push_token': registration_form['pushToken']}
            )
            if created:
                return Http201('')
            elif not registration.active:
                registration.active = True
                registration.save()
            pkpass_registered.send(sender=self.__class__)
            return Http200('')
        return Http400(registration_form.errors)

    @has_authorization
    def delete(self, request, *args, **kwargs):
        """
        DELETE request to v1/devices/{device_library_identifier}/registrations/
                                        {pass_type_identifier}/{serial_number}

        Parameters
            device_library_identifier
                A unique identifier that is used to identify and authenticate
                the device.

            pass_type_identifier
                The pass’s type, as specified in the pass.

            serial_number
                The unique pass identifier, as specified in the pass.

        Header
            The Authorization header is supplied; its value is the word
            “ApplePass”, followed by a space, followed by the pass’s
            authorization token as specified in the pass.

        Response
            If disassociation succeeds, return HTTP status 200.

            If the request is not authorized, return HTTP status 401.

            Otherwise, return the appropriate standard HTTP status.

        Discussion
            The server disassociates the specified device from the pass,
            and no longer send push notifications to this device when the
            pass changes.
        """
        registration = get_object_or_404(
            PkPassRegistration,
            device__device_library_identifier=self.kwargs[
                'device_library_identifier'
            ],
            pkpass__pass_type_identifier=self.kwargs['pass_type_identifier'],
            pkpass__serial_number=self.kwargs['serial_number']
        )
        registration.active = False
        registration.save()
        pkpass_unregistered.send(sender=self.__class__)
        return Http200('')


class PkPassRegistrationsEndpoint(Endpoint):
    """
    Endpoint for retrieve a list of passbook registered for a given device.
    """

    def get(self, request, *args, **kwargs):
        """
        GET request to v1/devices/{device_library_identifier}
                            /registrations/{pass_type_identifier}
                            ?passesUpdatedSince={tag}

        Parameters
            device_library_identifier
                A unique identifier that is used to identify and
                authenticate the device.

            pass_type_identifier
                The pass’s type, as specified in the pass.

            tag
                A tag from a previous request. (optional)
                If the passesUpdatedSince parameter is present, return only the
                passes that have been updated since the time indicated by tag.
                Otherwise, return all passes.

        Response
            If there are matching passes, return HTTP status 200 with a
            JSON dictionary with the following keys and values:
                lastUpdated (string)
                    The current modification tag.

                serialNumbers (array of strings)
                    The serial numbers of the matching passes.

            If there are no matching passes, return HTTP status 204.

            Otherwise, return the appropriate standard HTTP status.

        Discussion
            The modification tag is used to give a name to a point in time.
            It is typically convenient to use a timestamp, but the server is
            free to use another approach. The tag is treated as an opaque
            value by the system.
        """
        device = get_object_or_404(
            Device,
            device_library_identifier=self.kwargs[
                'device_library_identifier'
            ]
        )
        passes_updated_since = request.GET.get('passesUpdatedSince', None)
        registrations = PkPassRegistration.objects.filter(
            device=device,
            pkpass__pass_type_identifier=self.kwargs['pass_type_identifier'],
            active=True
        )

        if passes_updated_since:
            registrations = registrations.filter(
                pkpass__updated_at__gte=dateutil.parser.parse(
                    passes_updated_since
                ).replace(
                    tzinfo=dateutil.tz.tzutc()
                )
            )

        if not registrations.exists():
            return JSONResponse('', status=204)

        response = {
            'lastUpdated': timezone.now().isoformat(),
            'serialNumbers': list(
                registrations.values_list(
                    'pkpass__serial_number',
                    flat=True
                )
            )
        }

        return response


class PkPassLatestVersionEndpoint(Endpoint):
    """
    Endpoint for retrieve the latest version of a passbook.
    """

    @has_authorization
    def get(self, request, *args, **kwargs):
        """
        GET request to v1/passes/{pass_type_identifier}/{serial_number}

        Parameters
            pass_type_identifier
                The pass’s type, as specified in the pass.

            serial_number
                The unique pass identifier, as specified in the pass.

        Header
            The Authorization header is supplied;
            its value is the word “ApplePass”, followed by a space, followed by
            the pass’s authorization token as specified in the pass.

        Response
            If request is authorized, return HTTP status 200 with a payload of
            the pass data.

            If the request is not authorized, return HTTP status 401.

            Otherwise, return the appropriate standard HTTP status.

        Discussion
            Support standard HTTP caching on this endpoint: check for
            the If-Modified-Since header and return HTTP status code 304 if
            the pass has not changed.
        """
        pkpass = get_object_or_404(
            PkPass,
            pass_type_identifier=self.kwargs['pass_type_identifier'],
            serial_number=self.kwargs['serial_number']
        )
        response = HttpResponse(
            content_type='application/vnd.apple.pkpass'
        )
        response['Content-Disposition'] = 'attachment; filename=pass.pkpass'
        response.write(pkpass.zip())
        return response


class PkPassLogEndpoint(Endpoint):
    """
    Endpoint for debug your web service implementation.
    """

    def post(self, request, *args, **kwargs):
        """
        POST request to v1/log

        Payload
            The POST payload is a JSON dictionary,
            containing a single key and value:
                logs (string)
                    An array of log messages as strings.

        Response
            Return HTTP status 200.

        Discussion
            This endpoint is intended to help you debug your web service
            implementation. Log messages contain a description of the error
            in a human-readable format.
        """
        log_form = PkPassLogForm(request.data)
        if log_form.is_valid():
            PkPassLog.objects.create(message=log_form['logs'])
            return Http200('')
        return Http400(log_form.errors)
