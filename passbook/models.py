# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import io
import zipfile
import json
import hashlib
import subprocess

from tempfile import mkstemp

from django.conf import settings
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.contenttypes.models import ContentType

from colorful.fields import RGBColorField


def passbook_image_file_name(instance, filename):
    return "/".join(["passbook", str(instance.pk), 'images', filename])


def tempfile(data):
    temporary = mkstemp()
    with io.open(temporary[1], 'wb') as f:
        f.write(data.encode("utf-8"))
    return temporary[1]


@python_2_unicode_compatible
class PkPassBarcode(models.Model):
    """
    PkPass Barcode.
    """
    PDF417 = 'PKBarcodeFormatPDF417'
    QR = 'PKBarcodeFormatQR'
    AZTEC = 'PKBarcodeFormatAztec'
    FORMAT_CHOICES = (
        (PDF417, 'PDF 417'),
        (QR, 'QR'),
        (AZTEC, 'Aztec'),
    )
    barcode_format = models.CharField(max_length=32, choices=FORMAT_CHOICES)
    message = models.CharField(max_length=255)
    message_encoding = models.CharField(max_length=50, default='iso-8859-1')
    alt_text = models.CharField(max_length=255, blank=True, null=True)

    def serialize(self):
        result = {
            'format': self.barcode_format,
            'message': self.message,
            'messageEncoding': self.message_encoding,
        }

        if self.alt_text:
            result['altText'] = self.alt_text

        return result

    def __str__(self):
        return "Barcode [{barcode_format}] \"{message}\"".format(
            barcode_format=self.barcode_format,
            message=self.message
        )


@python_2_unicode_compatible
class PkPass(models.Model):
    """
    PkPass instance.
    """

    # Standard Keys
    description = models.CharField(max_length=255)
    format_version = 1
    organization_name = models.CharField(max_length=255)
    pass_type_identifier = models.CharField(max_length=255)
    serial_number = models.CharField(max_length=255)
    team_identifier = models.CharField(max_length=255)

    # Associated App Keys
    # TODO ver si agregar el campo appLaunchURL
    # TODO ver si agregar el campo associated_store_identifiers

    # Companion App Keys
    # TODO ver si agregar el campo user_info

    # Expiration Keys
    expiration_date = models.DateTimeField(null=True, blank=True)
    voided = models.NullBooleanField()

    # Relevance Keys
    # beacons (Related)
    # locations (Related)
    # TODO ver si agregar el campo maxDistance
    relevant_date = models.DateTimeField(null=True, blank=True)

    # Style Keys
    BOARDING_TYPE = 'boardingPass'
    COUPON_TYPE = 'coupon'
    EVENT_TICKET_TYPE = 'eventTicket'
    STORE_CARD_TYPE = 'storeCard'
    GENERIC_TYPE = 'generic'
    PASS_TYPE_CHOICES = (
        (BOARDING_TYPE, 'boarding pass'),
        (COUPON_TYPE, 'coupon'),
        (EVENT_TICKET_TYPE, 'event ticket'),
        (STORE_CARD_TYPE, 'store card'),
        (GENERIC_TYPE, 'generic'),
    )
    pass_type = models.CharField(max_length=32, choices=PASS_TYPE_CHOICES)
    AIR_TRANSIT_TYPE = 'PKTransitTypeAir'
    BOAT_TRANSIT_TYPE = 'PKTransitTypeBoat'
    BUS_TRANSIT_TYPE = 'PKTransitTypeBus'
    GENERIC_TRANSIT_TYPE = 'PKTransitTypeGeneric'
    TRAIN_TRANSIT_TYPE = 'PKTransitTypeTrain'
    TRANSIT_TYPE_CHOICES = (
        (AIR_TRANSIT_TYPE, 'Air'),
        (BOAT_TRANSIT_TYPE, 'Boat'),
        (BUS_TRANSIT_TYPE, 'Bus'),
        (GENERIC_TRANSIT_TYPE, 'Generic'),
        (TRAIN_TRANSIT_TYPE, 'Train'),
    )
    transit_type = models.CharField(
        max_length=32,
        choices=TRANSIT_TYPE_CHOICES,
        blank=True,
        null=True
    )  # Required for boarding passes

    # Visual Appearance Keys
    barcode = models.ForeignKey(PkPassBarcode, blank=True, null=True)
    background_color = RGBColorField(null=True, blank=True)
    foreground_color = RGBColorField(null=True, blank=True)
    # TODO ver si agregar el campo groupingIdentifier
    label_color = RGBColorField(null=True, blank=True)
    logo_text = models.CharField(
        max_length=50,
        null=True,
        blank=True
    )
    suppress_strip_shine = models.NullBooleanField()

    # Web Service Keys
    authentication_token = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    web_service_url = models.URLField(null=True, blank=True)

    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)

    logo_image = models.ImageField(
        upload_to=passbook_image_file_name,
        help_text=(
            'The logo image (logo.png) is displayed in the top '
            'left corner of the pass, next to the logo text'
        )
    )
    logo_image_2x = models.ImageField(
        upload_to=passbook_image_file_name,
        help_text=(
            'The logo image (logo.png) is displayed in the top '
            'left corner of the pass, next to the logo text'
        )
    )
    icon_image = models.ImageField(
        upload_to=passbook_image_file_name,
        help_text=(
            'The icon (icon.png) is displayed when a pass is shown on the '
            'lock screen and by apps such as Mail when showing a pass '
            'attached to an email'
        )
    )
    icon_image_2x = models.ImageField(
        upload_to=passbook_image_file_name,
        help_text=(
            'The icon (icon.png) is displayed when a pass is shown on the '
            'lock screen and by apps such as Mail when showing a pass '
            'attached to an email'
        )
    )
    thumbnail_image = models.ImageField(
        upload_to=passbook_image_file_name,
        help_text=(
            'The thumbnail image (thumbnail.png) displayed next to '
            'the fields on the front of the pass'
        ),
        null=True,
        blank=True
    )
    thumbnail_image_2x = models.ImageField(
        upload_to=passbook_image_file_name,
        help_text=(
            'The thumbnail image (thumbnail.png) displayed next to '
            'the fields on the front of the pass'
        ),
        null=True,
        blank=True
    )
    background_image = models.ImageField(
        upload_to=passbook_image_file_name,
        help_text=(
            'The image displayed as the background of '
            'the front of the pass'
        ),
        null=True,
        blank=True
    )
    background_image_2x = models.ImageField(
        upload_to=passbook_image_file_name,
        help_text=(
            'The image displayed as the background of '
            'the front of the pass'
        ),
        null=True,
        blank=True
    )
    strip_image = models.ImageField(
        upload_to=passbook_image_file_name,
        help_text=(
            'The strip image (strip.png) is displayed'
            ' behind the primary fields'
        ),
        null=True,
        blank=True
    )
    strip_image_2x = models.ImageField(
        upload_to=passbook_image_file_name,
        help_text=(
            'The strip image (strip.png) is displayed behind'
            ' the primary fields'
        ),
        null=True,
        blank=True
    )
    footer_image = models.ImageField(
        upload_to=passbook_image_file_name,
        help_text=(
            'The footer image (footer.png) is displayed near the barcode'
        ),
        null=True,
        blank=True
    )
    footer_image_2x = models.ImageField(
        upload_to=passbook_image_file_name,
        help_text=(
            'The footer image (footer.png) is displayed near the barcode'
        ),
        null=True,
        blank=True
    )

    class Meta:
        unique_together = (('pass_type_identifier', 'serial_number'),)

    def clean(self):
        super(PkPass, self).clean()
        if self.pass_type == PkPass.BOARDING_TYPE and not self.transit_type:
            raise ValidationError(
                'For a Boarding pass need to set a transit type.'
            )

        if self.web_service_url and not self.authentication_token:
            raise ValidationError(
                'If a web service URL is provided, an authentication token '
                'is required; otherwise, these keys are not allowed.'
            )

    def save(self, *args, **kwargs):
        self.clean()
        super(PkPass, self).save(*args, **kwargs)

    def serialize(self):
        result = {
            'description': self.description,
            'formatVersion': self.format_version,
            'organizationName': self.organization_name,
            'passTypeIdentifier': self.pass_type_identifier,
            'serialNumber': self.serial_number,
            'teamIdentifier': self.team_identifier,
            self.pass_type: {},
        }

        for key, value in PkPassField.FIELD_TYPE_CHOICES:
            if self.fields.filter(field_type=key).exists():
                result[self.pass_type][key] = [
                    field.cast().serialize() for field in self.fields.filter(
                        field_type=key
                    ).order_by('priority')
                ]

        if self.web_service_url and self.authentication_token:
            result['webServiceURL'] = self.web_service_url
            result['authenticationToken'] = self.authentication_token

        if self.locations.exists():
            result['locations'] = [
                location.serialize() for location in self.locations.all()
            ]

        if self.beacons.exists():
            result['beacons'] = [
                beacon.serialize() for beacon in self.beacons.all()
            ]

        if self.pass_type == 'boardingPass':
            result[self.pass_type]['transitType'] = self.transit_type

        if self.expiration_date:
            result['expirationDate'] = self.expiration_date.isoformat()

        if self.voided is not None:
            result['voided'] = self.voided

        if self.relevant_date:
            result['relevantDate'] = self.relevant_date.isoformat()

        if self.barcode:
            result['barcode'] = self.barcode.serialize()

        if self.background_color:
            result['backgroundColor'] = self.background_color

        if self.foreground_color:
            result['foregroundColor'] = self.foreground_color

        if self.label_color:
            result['labelColor'] = self.label_color

        if self.logo_text:
            result['logoText'] = self.logo_text

        if self.suppress_strip_shine is not None:
            result['suppressStripShine'] = self.suppress_strip_shine

        return json.dumps(result, indent=2)

    def _images(self):
        IMAGES = (
            ('logo_image', 'logo.png'),
            ('logo_image_2x', 'logo@2x.png'),
            ('icon_image', 'icon.png'),
            ('icon_image_2x', 'icon@2x.png'),
            ('thumbnail_image', 'thumbnail.png'),
            ('thumbnail_image_2x', 'thumbnail@2x.png'),
            ('background_image', 'background.png'),
            ('background_image_2x', 'background@2x.png'),
            ('strip_image', 'strip.png'),
            ('strip_image_2x', 'strip@2x.png'),
            ('footer_image', 'footer.png'),
            ('footer_image_2x', 'footer@2x.png'),
        )
        return [(attr, name) for attr, name in IMAGES if getattr(self, attr)]

    def manifest(self):
        manifest = {
            'pass.json': hashlib.sha1(
                self.serialize().encode("utf-8")
            ).hexdigest()
        }

        for attr, name in self._images():
            image = getattr(self, attr)
            image.open()
            manifest[name] = hashlib.sha1(image.read()).hexdigest()
            image.close()

        return json.dumps(manifest, indent=2)

    def sign(self):
        # TODO ver como hacer esto mejor xD
        key_file = tempfile(getattr(settings, 'PASSBOOK_KEY', 'some key'))
        cert_file = tempfile(
            getattr(settings, 'PASSBOOK_CERTIFICATE', 'some certificate')
        )
        wwdr_cert_file = tempfile(
            getattr(
                settings,
                'PASSBOOK_WWWDR_CERTIFICATE',
                'some wwdr_certificate'
            )
        )
        manifest_file = tempfile(self.manifest())

        command = (
            'openssl smime -binary -sign -signer {cert} -inkey {key}'
            ' -in {manifest} -outform DER -certfile {wwdr_cert} {passin}'
        ).format(
            cert=cert_file,
            key=key_file,
            manifest=manifest_file,
            wwdr_cert=wwdr_cert_file,
            passin='-passin pass:{passphrase}'.format(
                passphrase=getattr(
                    settings,
                    'PASSBOOK_PASSPHRASE'
                ) if getattr(settings, 'PASSBOOK_PASSPHRASE', False) else ''
            )
        )

        process = subprocess.Popen(
            command.split(),
            stderr=subprocess.PIPE,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE
        )
        signature = process.stdout.read()

        for f in (key_file, cert_file, wwdr_cert_file, manifest_file):
            os.remove(f)

        return signature

    def zip(self):
        out_file = io.BytesIO()
        pkpass = zipfile.ZipFile(out_file, 'a')
        pkpass.writestr('pass.json', self.serialize())
        pkpass.writestr('manifest.json', self.manifest())
        pkpass.writestr('signature', self.sign())

        for attr, name in self._images():
            image = getattr(self, attr)
            image.open()
            pkpass.writestr(name, image.read())
            image.close()

        for f in pkpass.filelist:
            f.create_system = 0
        pkpass.close()
        out_file.seek(0)

        return out_file.read()

    def __str__(self):
        return "PkPass {pass_type_identifier} \"{description}\"".format(
            pass_type_identifier=self.pass_type_identifier,
            description=self.description
        )


@python_2_unicode_compatible
class PkPassLocation(models.Model):
    """
    Location of interest for a pkpass.
    """
    latitude = models.FloatField()
    longitude = models.FloatField()
    altitude = models.FloatField(null=True)
    relevant_text = models.TextField(max_length=255, blank=True, null=True)

    pkpass = models.ForeignKey(PkPass, related_name='locations')

    def serialize(self):
        result = {
            'latitude': self.latitude,
            'longitude': self.longitude,
        }
        if self.altitude:
            result['altitude'] = self.altitude
        if self.relevant_text:
            result['relevantText'] = self.relevant_text

        return result

    def __str__(self):
        return "Location ({lat}, {lng}, {alt}) for {pkpass}".format(
            lat=self.longitude,
            lng=self.latitude,
            alt=self.altitude,
            pkpass=self.pkpass
        )


@python_2_unicode_compatible
class PkPassBeacon(models.Model):
    """
    Information about a location beacon.
    """
    major = models.IntegerField()
    minor = models.IntegerField()
    proximity_uuid = models.CharField(max_length=255)
    relevant_text = models.TextField(max_length=255, blank=True, null=True)

    pkpass = models.ForeignKey(PkPass, related_name='beacons')

    def serialize(self):
        result = {
            'major': self.major,
            'minor': self.minor,
            'proximityUUID': self.proximity_uuid,
        }
        if self.relevant_text:
            result['relevantText'] = self.relevant_text

        return result

    def __str__(self):
        return "Beacon ({minor}, {major}, {proximity}) for {pkpass}".format(
            major=self.major,
            minor=self.minor,
            proximity=self.proximity_uuid,
            pkpass=self.pkpass
        )


@python_2_unicode_compatible
class PkPassField(models.Model):
    """
    PkPass Field.
    """

    pkpass = models.ForeignKey(PkPass, related_name="fields")

    # Standard Field Dictionary Keys
    attributed_value = models.TextField(blank=True, null=True)
    change_message = models.CharField(
        max_length=255,
        blank=True,
        null=True
    )  # The format string must contain the escape %@,
    # which is replaced with the field’s new value.
    # For example, “Gate changed to %@.”
    PHONE_DETECTOR_TYPE = 'PKDataDetectorTypePhoneNumber'
    LINK_DETECTOR_TYPE = 'PKDataDetectorTypeLink'
    ADDRESS_DETECTOR_TYPE = 'PKDataDetectorTypeAddress'
    EVENT_DETECTOR_TYPE = 'PKDataDetectorTypeCalendarEvent'
    DATA_DETECTOR_TYPES_CHOICES = (
        (PHONE_DETECTOR_TYPE, 'Phone Number'),
        (LINK_DETECTOR_TYPE, 'Link'),
        (ADDRESS_DETECTOR_TYPE, 'Address'),
        (EVENT_DETECTOR_TYPE, 'Calendar Event'),
    )
    # TODO ver si agregar el campo data_detector_types
    key = models.CharField(max_length=255)
    label = models.CharField(max_length=255, blank=True, null=True)
    ALIGN_LEFT = 'PKTextAlignmentLeft'
    ALIGN_CENTER = 'PKTextAlignmentCenter'
    ALIGN_RIGHT = 'PKTextAlignmentRight'
    ALIGN_NATURAL = 'PKTextAlignmentNatural'
    TEXT_ALIGNMENT_CHOICES = (
        (ALIGN_LEFT, 'Alignment Left'),
        (ALIGN_CENTER, 'Alignment Center'),
        (ALIGN_RIGHT, 'Alignment Right'),
        (ALIGN_NATURAL, 'AlignmentNatural'),
    )
    text_alignment = models.CharField(
        max_length=32,
        choices=TEXT_ALIGNMENT_CHOICES,
        blank=True,
        null=True
    )
    value = models.TextField()

    # type of the field and order of appearance
    TYPE_HEADER = 'headerFields'
    TYPE_PRIMARY = 'primaryFields'
    TYPE_SECONDARY = 'secondaryFields'
    TYPE_AUXILIARY = 'auxiliaryFields'
    TYPE_BACK = 'backFields'
    FIELD_TYPE_CHOICES = (
        (TYPE_HEADER, 'Header Field'),
        (TYPE_PRIMARY, 'Primary Field'),
        (TYPE_SECONDARY, 'Secondary Field'),
        (TYPE_AUXILIARY, 'Auxiliary Field'),
        (TYPE_BACK, 'Back Field'),
    )
    field_type = models.CharField(max_length=32, choices=FIELD_TYPE_CHOICES)
    priority = models.PositiveSmallIntegerField(default=0)  # 0 is the highest

    real_type = models.ForeignKey(ContentType, editable=False)

    class Meta:
        unique_together = (('pkpass', 'key'),)

    def save(self, *args, **kwargs):
        if not self.id:
            self.real_type = self._get_real_type()
        super(PkPassField, self).save(*args, **kwargs)

    def _get_real_type(self):
        return ContentType.objects.get_for_model(type(self))

    def cast(self):
        return self.real_type.get_object_for_this_type(pk=self.pk)

    def serialize(self):
        result = {
            'key': self.key,
            'value': self.value,
        }

        if self.label:
            result['label'] = self.label
        if self.attributed_value:
            result['attributedValue'] = self.attributed_value
        if self.change_message:
            result['changeMessage'] = self.change_message
        if self.text_alignment:
            result['textAlignment'] = self.text_alignment

        return result

    def __str__(self):
        return "Field {{{key}, {value}}}".format(
            key=self.key,
            value=self.value
        )


@python_2_unicode_compatible
class DatePkPassField(PkPassField):
    """
    Date PkPass Field.
    """
    # Date Style Keys
    DATE_STYLE_NONE = 'PKDateStyleNone'
    DATE_STYLE_SHORT = 'PKDateStyleShort'
    DATE_STYLE_MEDIUM = 'PKDateStyleMedium'
    DATE_STYLE_LONG = 'PKDateStyleLong'
    DATE_STYLE_FULL = 'PKDateStyleFull'
    DATE_STYLES_CHOICES = (
        (DATE_STYLE_NONE, 'Style None '),
        (DATE_STYLE_SHORT, 'Style Short'),
        (DATE_STYLE_MEDIUM, 'Style Medium'),
        (DATE_STYLE_LONG, 'Style Long'),
        (DATE_STYLE_FULL, 'Style Full'),
    )
    date_style = models.CharField(max_length=32, choices=DATE_STYLES_CHOICES)
    ignores_time_zone = models.NullBooleanField()
    is_relative = models.NullBooleanField()
    time_style = models.CharField(max_length=32, choices=DATE_STYLES_CHOICES)

    def serialize(self):
        result = super(DatePkPassField, self).serialize()
        result.update({
            'dateStyle': self.date_style,
            'timeStyle': self.time_style,
        })

        if self.ignores_time_zone is not None:
            result['ignoresTimeZone'] = self.ignores_time_zone
        if self.is_relative is not None:
            result['isRelative'] = self.is_relative

        return result

    def __str__(self):
        return "Field {{{key}, {value}}} of {date_style}".format(
            key=self.key,
            value=self.value,
            date_style=self.date_style
        )


@python_2_unicode_compatible
class NumberPkPassField(PkPassField):
    """
    Number PkPass Field.
    """
    STYLE_DECIMAL = 'PKNumberStyleDecimal'
    STYLE_PERCENT = 'PKNumberStylePercent'
    STYLE_SCIENTIFIC = 'PKNumberStyleScientific'
    STYLE_SPELL_OUT = 'PKNumberStyleSpellOut'
    NUMBERS_STYLES_CHOICES = (
        (STYLE_DECIMAL, 'Decimal'),
        (STYLE_PERCENT, 'Percent'),
        (STYLE_SCIENTIFIC, 'Scientific'),
        (STYLE_SPELL_OUT, 'Spell Out'),
    )
    number_style = models.CharField(
        max_length=32,
        choices=NUMBERS_STYLES_CHOICES
    )

    def serialize(self):
        result = super(NumberPkPassField, self).serialize()
        result.update({
            'numberStyle': self.number_style,
        })

        return result

    def __str__(self):
        return "Field {{{key}, {value}}} of {number_style}".format(
            key=self.key,
            value=self.value,
            number_style=self.number_style
        )


@python_2_unicode_compatible
class CurrencyPkPassField(PkPassField):
    """
    Currency PkPass Field.
    """
    # TODO ver de hacer choices
    currency_code = models.CharField(max_length=5)  # ISO 4217 currency code

    def serialize(self):
        result = super(CurrencyPkPassField, self).serialize()
        result.update({
            'currencyCode': self.currency_code,
        })

        return result

    def __str__(self):
        return "Field {{{key}, {value}}} of {code}".format(
            key=self.key,
            value=self.value,
            code=self.currency_code
        )


@python_2_unicode_compatible
class Device(models.Model):
    """
    Apple device.
    """

    device_library_identifier = models.CharField(max_length=255, unique=True)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "Apple device {device}".format(
            device=self.device_library_identifier
        )


@python_2_unicode_compatible
class PkPassRegistration(models.Model):
    """
    Registration of a PkPass.
    """

    device = models.ForeignKey(Device)
    pkpass = models.ForeignKey(PkPass)
    push_token = models.CharField(max_length=255)
    created_at = models.DateTimeField(default=timezone.now)

    active = models.BooleanField(default=True)

    def __str__(self):
        return "Registration for {pkpass} from {device} at {date}".format(
            pkpass=self.pkpass,
            device=self.device,
            date=self.created_at
        )


@python_2_unicode_compatible
class PkPassLog(models.Model):
    """
    Log message sent by a device.
    """
    message = models.TextField()
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "[{time}] {message}".format(
            time=self.created_at.strftime("%Y-%m-%d %H:%M:%S"),
            message=self.message
        )
