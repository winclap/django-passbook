# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PkPassBarcode'
        db.create_table(u'passbook_pkpassbarcode', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('barcode_format', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('message', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('message_encoding', self.gf('django.db.models.fields.CharField')(default=u'iso-8859-1', max_length=50)),
            ('alt_text', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'passbook', ['PkPassBarcode'])

        # Adding model 'PkPass'
        db.create_table(u'passbook_pkpass', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('organization_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('pass_type_identifier', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('serial_number', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('team_identifier', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('expiration_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('voided', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('relevant_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('pass_type', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('transit_type', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('barcode', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['passbook.PkPassBarcode'], null=True, blank=True)),
            ('background_color', self.gf(u'colorful.fields.RGBColorField')(null=True, blank=True)),
            ('foreground_color', self.gf(u'colorful.fields.RGBColorField')(null=True, blank=True)),
            ('label_color', self.gf(u'colorful.fields.RGBColorField')(null=True, blank=True)),
            ('logo_text', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('suppress_strip_shine', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('authentication_token', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('web_service_url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('logo_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('logo_image_2x', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('icon_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('icon_image_2x', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('thumbnail_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('thumbnail_image_2x', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('background_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('background_image_2x', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('strip_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('strip_image_2x', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('footer_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('footer_image_2x', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'passbook', ['PkPass'])

        # Adding unique constraint on 'PkPass', fields ['pass_type_identifier', 'serial_number']
        db.create_unique(u'passbook_pkpass', ['pass_type_identifier', 'serial_number'])

        # Adding model 'PkPassLocation'
        db.create_table(u'passbook_pkpasslocation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('latitude', self.gf('django.db.models.fields.FloatField')()),
            ('longitude', self.gf('django.db.models.fields.FloatField')()),
            ('altitude', self.gf('django.db.models.fields.FloatField')(null=True)),
            ('relevant_text', self.gf('django.db.models.fields.TextField')(max_length=255, null=True, blank=True)),
            ('pkpass', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'locations', to=orm['passbook.PkPass'])),
        ))
        db.send_create_signal(u'passbook', ['PkPassLocation'])

        # Adding model 'PkPassBeacon'
        db.create_table(u'passbook_pkpassbeacon', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('major', self.gf('django.db.models.fields.IntegerField')()),
            ('minor', self.gf('django.db.models.fields.IntegerField')()),
            ('proximity_uuid', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('relevant_text', self.gf('django.db.models.fields.TextField')(max_length=255, null=True, blank=True)),
            ('pkpass', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'beacons', to=orm['passbook.PkPass'])),
        ))
        db.send_create_signal(u'passbook', ['PkPassBeacon'])

        # Adding model 'PkPassField'
        db.create_table(u'passbook_pkpassfield', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pkpass', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'fields', to=orm['passbook.PkPass'])),
            ('attributed_value', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('change_message', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('key', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('label', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('text_alignment', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('value', self.gf('django.db.models.fields.TextField')()),
            ('field_type', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('priority', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('real_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
        ))
        db.send_create_signal(u'passbook', ['PkPassField'])

        # Adding unique constraint on 'PkPassField', fields ['pkpass', 'key']
        db.create_unique(u'passbook_pkpassfield', ['pkpass_id', 'key'])

        # Adding model 'DatePkPassField'
        db.create_table(u'passbook_datepkpassfield', (
            (u'pkpassfield_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['passbook.PkPassField'], unique=True, primary_key=True)),
            ('date_style', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('ignores_time_zone', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('is_relative', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('time_style', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal(u'passbook', ['DatePkPassField'])

        # Adding model 'NumberPkPassField'
        db.create_table(u'passbook_numberpkpassfield', (
            (u'pkpassfield_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['passbook.PkPassField'], unique=True, primary_key=True)),
            ('number_style', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal(u'passbook', ['NumberPkPassField'])

        # Adding model 'CurrencyPkPassField'
        db.create_table(u'passbook_currencypkpassfield', (
            (u'pkpassfield_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['passbook.PkPassField'], unique=True, primary_key=True)),
            ('currency_code', self.gf('django.db.models.fields.CharField')(max_length=5)),
        ))
        db.send_create_signal(u'passbook', ['CurrencyPkPassField'])

        # Adding model 'Device'
        db.create_table(u'passbook_device', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('device_library_identifier', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
        ))
        db.send_create_signal(u'passbook', ['Device'])

        # Adding model 'PkPassRegistration'
        db.create_table(u'passbook_pkpassregistration', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('device', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['passbook.Device'])),
            ('pkpass', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['passbook.PkPass'])),
            ('push_token', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'passbook', ['PkPassRegistration'])

        # Adding model 'PkPassLog'
        db.create_table(u'passbook_pkpasslog', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('message', self.gf('django.db.models.fields.TextField')()),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
        ))
        db.send_create_signal(u'passbook', ['PkPassLog'])


    def backwards(self, orm):
        # Removing unique constraint on 'PkPassField', fields ['pkpass', 'key']
        db.delete_unique(u'passbook_pkpassfield', ['pkpass_id', 'key'])

        # Removing unique constraint on 'PkPass', fields ['pass_type_identifier', 'serial_number']
        db.delete_unique(u'passbook_pkpass', ['pass_type_identifier', 'serial_number'])

        # Deleting model 'PkPassBarcode'
        db.delete_table(u'passbook_pkpassbarcode')

        # Deleting model 'PkPass'
        db.delete_table(u'passbook_pkpass')

        # Deleting model 'PkPassLocation'
        db.delete_table(u'passbook_pkpasslocation')

        # Deleting model 'PkPassBeacon'
        db.delete_table(u'passbook_pkpassbeacon')

        # Deleting model 'PkPassField'
        db.delete_table(u'passbook_pkpassfield')

        # Deleting model 'DatePkPassField'
        db.delete_table(u'passbook_datepkpassfield')

        # Deleting model 'NumberPkPassField'
        db.delete_table(u'passbook_numberpkpassfield')

        # Deleting model 'CurrencyPkPassField'
        db.delete_table(u'passbook_currencypkpassfield')

        # Deleting model 'Device'
        db.delete_table(u'passbook_device')

        # Deleting model 'PkPassRegistration'
        db.delete_table(u'passbook_pkpassregistration')

        # Deleting model 'PkPassLog'
        db.delete_table(u'passbook_pkpasslog')


    models = {
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'passbook.currencypkpassfield': {
            'Meta': {'object_name': 'CurrencyPkPassField', '_ormbases': [u'passbook.PkPassField']},
            'currency_code': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            u'pkpassfield_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['passbook.PkPassField']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'passbook.datepkpassfield': {
            'Meta': {'object_name': 'DatePkPassField', '_ormbases': [u'passbook.PkPassField']},
            'date_style': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'ignores_time_zone': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'is_relative': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            u'pkpassfield_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['passbook.PkPassField']", 'unique': 'True', 'primary_key': 'True'}),
            'time_style': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'passbook.device': {
            'Meta': {'object_name': 'Device'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'device_library_identifier': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'passbook.numberpkpassfield': {
            'Meta': {'object_name': 'NumberPkPassField', '_ormbases': [u'passbook.PkPassField']},
            'number_style': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'pkpassfield_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['passbook.PkPassField']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'passbook.pkpass': {
            'Meta': {'unique_together': "((u'pass_type_identifier', u'serial_number'),)", 'object_name': 'PkPass'},
            'authentication_token': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'background_color': (u'colorful.fields.RGBColorField', [], {'null': 'True', 'blank': 'True'}),
            'background_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'background_image_2x': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'barcode': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['passbook.PkPassBarcode']", 'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'expiration_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'footer_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'footer_image_2x': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'foreground_color': (u'colorful.fields.RGBColorField', [], {'null': 'True', 'blank': 'True'}),
            'icon_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'icon_image_2x': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label_color': (u'colorful.fields.RGBColorField', [], {'null': 'True', 'blank': 'True'}),
            'logo_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'logo_image_2x': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'logo_text': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'organization_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'pass_type': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pass_type_identifier': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'relevant_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'serial_number': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'strip_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'strip_image_2x': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'suppress_strip_shine': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'team_identifier': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'thumbnail_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'thumbnail_image_2x': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'transit_type': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'voided': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'web_service_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'passbook.pkpassbarcode': {
            'Meta': {'object_name': 'PkPassBarcode'},
            'alt_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'barcode_format': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'message_encoding': ('django.db.models.fields.CharField', [], {'default': "u'iso-8859-1'", 'max_length': '50'})
        },
        u'passbook.pkpassbeacon': {
            'Meta': {'object_name': 'PkPassBeacon'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'major': ('django.db.models.fields.IntegerField', [], {}),
            'minor': ('django.db.models.fields.IntegerField', [], {}),
            'pkpass': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'beacons'", 'to': u"orm['passbook.PkPass']"}),
            'proximity_uuid': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'relevant_text': ('django.db.models.fields.TextField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'passbook.pkpassfield': {
            'Meta': {'unique_together': "((u'pkpass', u'key'),)", 'object_name': 'PkPassField'},
            'attributed_value': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'change_message': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'field_type': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'pkpass': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'fields'", 'to': u"orm['passbook.PkPass']"}),
            'priority': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'real_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            'text_alignment': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.TextField', [], {})
        },
        u'passbook.pkpasslocation': {
            'Meta': {'object_name': 'PkPassLocation'},
            'altitude': ('django.db.models.fields.FloatField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.FloatField', [], {}),
            'longitude': ('django.db.models.fields.FloatField', [], {}),
            'pkpass': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'locations'", 'to': u"orm['passbook.PkPass']"}),
            'relevant_text': ('django.db.models.fields.TextField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'passbook.pkpasslog': {
            'Meta': {'object_name': 'PkPassLog'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {})
        },
        u'passbook.pkpassregistration': {
            'Meta': {'object_name': 'PkPassRegistration'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['passbook.Device']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pkpass': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['passbook.PkPass']"}),
            'push_token': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['passbook']
