# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms


class PkPassRegistrationForm(forms.Form):

    pushToken = forms.CharField()


class PkPassLogForm(forms.Form):

    logs = forms.CharField()
