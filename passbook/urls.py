# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(
        r'^v1/',
        include([
            url(
                (
                    r'^devices/(?P<device_library_identifier>[-\w]+)/'
                    'registrations/(?P<pass_type_identifier>[-\w\.]+)/'
                ),
                include([
                    url(
                        r'^(?P<serial_number>[-\w]+)/$',
                        views.PkPassRegisterDeleteEndpoint.as_view(),
                        name='passbook_register_delete'
                    ),
                    url(
                        r'^$',
                        views.PkPassRegistrationsEndpoint.as_view(),
                        name='passbook_registrations'
                    ),
                ])
            ),
            url(
                (
                    r'^passes/(?P<pass_type_identifier>[-\w\.]+)/'
                    '(?P<serial_number>[-\w]+)/$'
                ),
                views.PkPassLatestVersionEndpoint.as_view(),
                name='passbook_latest'
            ),
            url(
                r'^log/$',
                views.PkPassLogEndpoint.as_view(),
                name='passbook_log'
            ),
        ])
    ),
]
