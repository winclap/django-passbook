# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from restless.http import Http401

from .models import PkPass


def has_authorization(fn):
    """
    Decorator for :py:class:`restless.views.Endpoint` methods to require
    ApplePass authorization code.
    """
    def wrapper(self, request, *args, **kwargs):
        serial_number = kwargs.get('serial_number')
        authorization = request.META.get('HTTP_AUTHORIZATION', '')
        if serial_number and authorization.startswith('ApplePass '):
            if PkPass.objects.filter(
                   serial_number=serial_number,
                   authentication_token=authorization.split(' ')[1].strip()
               ).exists():
                return fn(self, request, *args, **kwargs)
        return Http401()
    wrapper.__name__ = fn.__name__
    wrapper.__doc__ = fn.__doc__
    return wrapper
