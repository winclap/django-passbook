# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import colorful.fields
import passbook.models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Device',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('device_library_identifier', models.CharField(unique=True, max_length=255)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='PkPass',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=255)),
                ('organization_name', models.CharField(max_length=255)),
                ('pass_type_identifier', models.CharField(max_length=255)),
                ('serial_number', models.CharField(max_length=255)),
                ('team_identifier', models.CharField(max_length=255)),
                ('expiration_date', models.DateTimeField(null=True, blank=True)),
                ('voided', models.NullBooleanField()),
                ('relevant_date', models.DateTimeField(null=True, blank=True)),
                ('pass_type', models.CharField(max_length=32, choices=[('boardingPass', 'boarding pass'), ('coupon', 'coupon'), ('eventTicket', 'event ticket'), ('storeCard', 'store card'), ('generic', 'generic')])),
                ('transit_type', models.CharField(blank=True, max_length=32, null=True, choices=[('PKTransitTypeAir', 'Air'), ('PKTransitTypeBoat', 'Boat'), ('PKTransitTypeBus', 'Bus'), ('PKTransitTypeGeneric', 'Generic'), ('PKTransitTypeTrain', 'Train')])),
                ('background_color', colorful.fields.RGBColorField(null=True, blank=True)),
                ('foreground_color', colorful.fields.RGBColorField(null=True, blank=True)),
                ('label_color', colorful.fields.RGBColorField(null=True, blank=True)),
                ('logo_text', models.CharField(max_length=50, null=True, blank=True)),
                ('suppress_strip_shine', models.NullBooleanField()),
                ('authentication_token', models.CharField(max_length=255, null=True, blank=True)),
                ('web_service_url', models.URLField(null=True, blank=True)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('logo_image', models.ImageField(help_text='The logo image (logo.png) is displayed in the top left corner of the pass, next to the logo text', upload_to=passbook.models.passbook_image_file_name)),
                ('logo_image_2x', models.ImageField(help_text='The logo image (logo.png) is displayed in the top left corner of the pass, next to the logo text', upload_to=passbook.models.passbook_image_file_name)),
                ('icon_image', models.ImageField(help_text='The icon (icon.png) is displayed when a pass is shown on the lock screen and by apps such as Mail when showing a pass attached to an email', upload_to=passbook.models.passbook_image_file_name)),
                ('icon_image_2x', models.ImageField(help_text='The icon (icon.png) is displayed when a pass is shown on the lock screen and by apps such as Mail when showing a pass attached to an email', upload_to=passbook.models.passbook_image_file_name)),
                ('thumbnail_image', models.ImageField(help_text='The thumbnail image (thumbnail.png) displayed next to the fields on the front of the pass', null=True, upload_to=passbook.models.passbook_image_file_name, blank=True)),
                ('thumbnail_image_2x', models.ImageField(help_text='The thumbnail image (thumbnail.png) displayed next to the fields on the front of the pass', null=True, upload_to=passbook.models.passbook_image_file_name, blank=True)),
                ('background_image', models.ImageField(help_text='The image displayed as the background of the front of the pass', null=True, upload_to=passbook.models.passbook_image_file_name, blank=True)),
                ('background_image_2x', models.ImageField(help_text='The image displayed as the background of the front of the pass', null=True, upload_to=passbook.models.passbook_image_file_name, blank=True)),
                ('strip_image', models.ImageField(help_text='The strip image (strip.png) is displayed behind the primary fields', null=True, upload_to=passbook.models.passbook_image_file_name, blank=True)),
                ('strip_image_2x', models.ImageField(help_text='The strip image (strip.png) is displayed behind the primary fields', null=True, upload_to=passbook.models.passbook_image_file_name, blank=True)),
                ('footer_image', models.ImageField(help_text='The footer image (footer.png) is displayed near the barcode', null=True, upload_to=passbook.models.passbook_image_file_name, blank=True)),
                ('footer_image_2x', models.ImageField(help_text='The footer image (footer.png) is displayed near the barcode', null=True, upload_to=passbook.models.passbook_image_file_name, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='PkPassBarcode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('barcode_format', models.CharField(max_length=32, choices=[('PKBarcodeFormatPDF417', 'PDF 417'), ('PKBarcodeFormatQR', 'QR'), ('PKBarcodeFormatAztec', 'Aztec')])),
                ('message', models.CharField(max_length=255)),
                ('message_encoding', models.CharField(default='iso-8859-1', max_length=50)),
                ('alt_text', models.CharField(max_length=255, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='PkPassBeacon',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('major', models.IntegerField()),
                ('minor', models.IntegerField()),
                ('proximity_uuid', models.CharField(max_length=255)),
                ('relevant_text', models.TextField(max_length=255, null=True, blank=True)),
                ('pkpass', models.ForeignKey(related_name='beacons', to='passbook.PkPass')),
            ],
        ),
        migrations.CreateModel(
            name='PkPassField',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('attributed_value', models.TextField(null=True, blank=True)),
                ('change_message', models.CharField(max_length=255, null=True, blank=True)),
                ('key', models.CharField(max_length=255)),
                ('label', models.CharField(max_length=255, null=True, blank=True)),
                ('text_alignment', models.CharField(blank=True, max_length=32, null=True, choices=[('PKTextAlignmentLeft', 'Alignment Left'), ('PKTextAlignmentCenter', 'Alignment Center'), ('PKTextAlignmentRight', 'Alignment Right'), ('PKTextAlignmentNatural', 'AlignmentNatural')])),
                ('value', models.TextField()),
                ('field_type', models.CharField(max_length=32, choices=[('headerFields', 'Header Field'), ('primaryFields', 'Primary Field'), ('secondaryFields', 'Secondary Field'), ('auxiliaryFields', 'Auxiliary Field'), ('backFields', 'Back Field')])),
                ('priority', models.PositiveSmallIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='PkPassLocation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('latitude', models.FloatField()),
                ('longitude', models.FloatField()),
                ('altitude', models.FloatField(null=True)),
                ('relevant_text', models.TextField(max_length=255, null=True, blank=True)),
                ('pkpass', models.ForeignKey(related_name='locations', to='passbook.PkPass')),
            ],
        ),
        migrations.CreateModel(
            name='PkPassLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('message', models.TextField()),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='PkPassRegistration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('push_token', models.CharField(max_length=255)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('active', models.BooleanField(default=True)),
                ('device', models.ForeignKey(to='passbook.Device')),
                ('pkpass', models.ForeignKey(to='passbook.PkPass')),
            ],
        ),
        migrations.CreateModel(
            name='CurrencyPkPassField',
            fields=[
                ('pkpassfield_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='passbook.PkPassField')),
                ('currency_code', models.CharField(max_length=5)),
            ],
            bases=('passbook.pkpassfield',),
        ),
        migrations.CreateModel(
            name='DatePkPassField',
            fields=[
                ('pkpassfield_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='passbook.PkPassField')),
                ('date_style', models.CharField(max_length=32, choices=[('PKDateStyleNone', 'Style None '), ('PKDateStyleShort', 'Style Short'), ('PKDateStyleMedium', 'Style Medium'), ('PKDateStyleLong', 'Style Long'), ('PKDateStyleFull', 'Style Full')])),
                ('ignores_time_zone', models.NullBooleanField()),
                ('is_relative', models.NullBooleanField()),
                ('time_style', models.CharField(max_length=32, choices=[('PKDateStyleNone', 'Style None '), ('PKDateStyleShort', 'Style Short'), ('PKDateStyleMedium', 'Style Medium'), ('PKDateStyleLong', 'Style Long'), ('PKDateStyleFull', 'Style Full')])),
            ],
            bases=('passbook.pkpassfield',),
        ),
        migrations.CreateModel(
            name='NumberPkPassField',
            fields=[
                ('pkpassfield_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='passbook.PkPassField')),
                ('number_style', models.CharField(max_length=32, choices=[('PKNumberStyleDecimal', 'Decimal'), ('PKNumberStylePercent', 'Percent'), ('PKNumberStyleScientific', 'Scientific'), ('PKNumberStyleSpellOut', 'Spell Out')])),
            ],
            bases=('passbook.pkpassfield',),
        ),
        migrations.AddField(
            model_name='pkpassfield',
            name='pkpass',
            field=models.ForeignKey(related_name='fields', to='passbook.PkPass'),
        ),
        migrations.AddField(
            model_name='pkpassfield',
            name='real_type',
            field=models.ForeignKey(editable=False, to='contenttypes.ContentType'),
        ),
        migrations.AddField(
            model_name='pkpass',
            name='barcode',
            field=models.ForeignKey(blank=True, to='passbook.PkPassBarcode', null=True),
        ),
        migrations.AlterUniqueTogether(
            name='pkpassfield',
            unique_together=set([('pkpass', 'key')]),
        ),
        migrations.AlterUniqueTogether(
            name='pkpass',
            unique_together=set([('pass_type_identifier', 'serial_number')]),
        ),
    ]
