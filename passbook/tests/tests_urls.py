# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import binascii

from django.test import TestCase
from django.core.urlresolvers import reverse, resolve

from .. import views


class PassbookUrlsTestCase(TestCase):
    """
    Test for urls.py
    """

    def setUp(self):
        """
        Seting up some common varaibles.
        """
        self.device_id = binascii.b2a_hex(os.urandom(15)).decode()
        self.pass_type_id = binascii.b2a_hex(os.urandom(15)).decode()
        self.serial_number = binascii.b2a_hex(os.urandom(4)).decode()

        self.register_delete_url_format = (
            '/v1/devices/{device_id}/'
            'registrations/{pass_type_id}/{serial_number}/'
        )
        self.registrations_url_format = (
            '/v1/devices/{device_id}/'
            'registrations/{pass_type_id}/'
        )
        self.latest_url_format = '/v1/passes/{pass_type_id}/{serial_number}/'
        self.log_url = '/v1/log/'

    def test_register_delete_url_reverse_name(self):
        expected_url = self.register_delete_url_format
        url = reverse(
            'passbook_register_delete',
            kwargs={
                'device_library_identifier': self.device_id,
                'pass_type_identifier': self.pass_type_id,
                'serial_number': self.serial_number,
            }
        )
        self.assertEqual(
            url,
            expected_url.format(
                device_id=self.device_id,
                pass_type_id=self.pass_type_id,
                serial_number=self.serial_number
            )
        )

    def test_registrations_url_reverse_name(self):
        url = reverse(
            'passbook_registrations',
            kwargs={
                'device_library_identifier': self.device_id,
                'pass_type_identifier': self.pass_type_id,
            }
        )
        self.assertEqual(
            url,
            self.registrations_url_format.format(
                device_id=self.device_id,
                pass_type_id=self.pass_type_id)
        )

    def test_latest_version_url_reverse_name(self):
        url = reverse(
            'passbook_latest',
            kwargs={
                'pass_type_identifier': self.pass_type_id,
                'serial_number': self.serial_number,
            }
        )
        self.assertEqual(
            url,
            self.latest_url_format.format(
                pass_type_id=self.pass_type_id,
                serial_number=self.serial_number
            )
        )

    def test_log_url_reverse_name(self):
        url = reverse('passbook_log')
        self.assertEqual(url, self.log_url)

    def test_register_delete_url_resolves_to_register_delete_endpoint(self):
        found = resolve(
            self.register_delete_url_format.format(
                device_id=self.device_id,
                pass_type_id=self.pass_type_id,
                serial_number=self.serial_number
            )
        )
        self.assertEqual(
            found.func.__name__,
            views.PkPassRegisterDeleteEndpoint.as_view().__name__
        )

    def test_registrations_url_resolves_to_registrations_endpoint(self):
        found = resolve(
            self.registrations_url_format.format(
                device_id=self.device_id,
                pass_type_id=self.pass_type_id
            )
        )
        self.assertEqual(
            found.func.__name__,
            views.PkPassRegistrationsEndpoint.as_view().__name__
        )

    def test_latest_version_url_resolves_to_latest_version_endpoint(self):
        found = resolve(
            self.latest_url_format.format(
                pass_type_id=self.pass_type_id,
                serial_number=self.serial_number
            )
        )
        self.assertEqual(
            found.func.__name__,
            views.PkPassLatestVersionEndpoint.as_view().__name__
        )

    def test_log_url_resolves_to_log_endpoint(self):
        found = resolve(
            self.log_url
        )
        self.assertEqual(
            found.func.__name__,
            views.PkPassLogEndpoint.as_view().__name__
        )
