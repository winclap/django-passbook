# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from django.utils import timezone
from django.core.exceptions import ValidationError

from ..models import PkPass


class PkPassTestCase(TestCase):

    def setUp(self):
        self.pass_type_id = 'pass.com.example.boardingpass'
        self.serial_number = 'E5982H-I2'

        self.auth_token = 'vxwxd7J8AlNNFPS8k0a0FfUFtq0ewzFdc'

    def test_fail_boarding_pass_without_transit_type(self):
        with self.assertRaisesMessage(
            ValidationError,
            'For a Boarding pass need to set a transit type.'
        ):
            PkPass.objects.create(
                description='20% off premium dog food',
                organization_name='Paw Planet',
                pass_type_identifier=self.pass_type_id,
                serial_number=self.serial_number,
                team_identifier='A1B2C3D4E5',
                pass_type=PkPass.BOARDING_TYPE,
                authentication_token=self.auth_token,
                web_service_url='http://localhost:8000/passbook/',
                logo_text='Paw Planet',
                expiration_date=(
                        timezone.now() + timezone.timedelta(30)
                ).isoformat(),
                voided=True,
                background_color='#000000',
                foreground_color='#FFFFFF',
                label_color='#FFFFFF',
                suppress_strip_shine=False
            )
