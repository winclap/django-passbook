# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import binascii
import json

from django.test import TestCase, Client
from django.utils import timezone

from ..models import (
    PkPass,
    Device,
    PkPassRegistration,
    PkPassBarcode,
    PkPassLocation,
    PkPassBeacon,
    DatePkPassField,
    NumberPkPassField,
    CurrencyPkPassField,
    PkPassField
)


class PkPassRegisterEndpointTestCase(TestCase):

    def setUp(self):
        self.register_delete_url_format = (
            '/v1/devices/{device_id}/' +
            'registrations/{pass_type_id}/{serial_number}/'
        )
        self.client = Client()
        self.device_id = binascii.b2a_hex(os.urandom(15)).decode()
        self.pass_type_id = 'pass.com.example.coupon'
        self.serial_number = 'E5982H-I2'

        self.push_token = binascii.b2a_hex(os.urandom(15)).decode()

        self.auth_token = 'vxwxd7J8AlNNFPS8k0a0FfUFtq0ewzFdc'

        PkPass.objects.create(
            description='20% off premium dog food',
            organization_name='Paw Planet',
            pass_type_identifier=self.pass_type_id,
            serial_number=self.serial_number,
            team_identifier='A1B2C3D4E5',
            pass_type=PkPass.COUPON_TYPE,
            authentication_token=self.auth_token,
            web_service_url='http://localhost:8000/passbook/',
            logo_text='Paw Planet'
        )

    def test_correct_registration(self):
        response = self.client.post(
            self.register_delete_url_format.format(
                device_id=self.device_id,
                pass_type_id=self.pass_type_id,
                serial_number=self.serial_number
            ),
            {'pushToken': self.push_token},
            **{
                'HTTP_AUTHORIZATION': 'ApplePass {auth_token}'.format(
                    auth_token=self.auth_token
                ),
            }
        )

        # Check that the response is 201 CREATED.
        self.assertEqual(response.status_code, 201)

        # Detele only for check
        response = self.client.delete(
            self.register_delete_url_format.format(
                device_id=self.device_id,
                pass_type_id=self.pass_type_id,
                serial_number=self.serial_number
            ),
            **{
                'HTTP_AUTHORIZATION': 'ApplePass {auth_token}'.format(
                    auth_token=self.auth_token
                ),
            }
        )

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

        response = self.client.post(
            self.register_delete_url_format.format(
                device_id=self.device_id,
                pass_type_id=self.pass_type_id,
                serial_number=self.serial_number
            ),
            {'pushToken': self.push_token},
            **{
                'HTTP_AUTHORIZATION': 'ApplePass {auth_token}'.format(
                    auth_token=self.auth_token
                ),
            }
        )

        # Now Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

    def test_bad_request_registration(self):
        response = self.client.post(
            self.register_delete_url_format.format(
                device_id=self.device_id,
                pass_type_id=self.pass_type_id,
                serial_number=self.serial_number
            ),
            {'wrong_key': self.push_token},
            **{
                'HTTP_AUTHORIZATION': 'ApplePass {auth_token}'.format(
                    auth_token=self.auth_token
                ),
            }
        )

        # Check that the response is 400 Bad Request.
        self.assertEqual(response.status_code, 400)

    def test_not_authorized_registration(self):
        response = self.client.post(
            self.register_delete_url_format.format(
                device_id=self.device_id,
                pass_type_id=self.pass_type_id,
                serial_number=self.serial_number
            ),
            {'pushToken': self.push_token},
            **{
                'HTTP_AUTHORIZATION': 'ApplePass {auth_token}'.format(
                    auth_token='BadToken'
                ),
            }
        )

        # Check that the response is 401 UNAUTHENTICATED.
        self.assertEqual(response.status_code, 401)


class PkPassDeleteEndpointTestCase(TestCase):

    def setUp(self):
        self.register_delete_url_format = (
            '/v1/devices/{device_id}/' +
            'registrations/{pass_type_id}/{serial_number}/'
        )
        self.client = Client()
        self.device_id = binascii.b2a_hex(os.urandom(15)).decode()
        self.pass_type_id = 'pass.com.example.coupon'
        self.serial_number = 'E5982H-I2'

        self.push_token = binascii.b2a_hex(os.urandom(15)).decode()

        self.auth_token = 'vxwxd7J8AlNNFPS8k0a0FfUFtq0ewzFdc'

        pkpass = PkPass.objects.create(
            description='20% off premium dog food',
            organization_name='Paw Planet',
            pass_type_identifier=self.pass_type_id,
            serial_number=self.serial_number,
            team_identifier='A1B2C3D4E5',
            pass_type=PkPass.COUPON_TYPE,
            authentication_token=self.auth_token,
            web_service_url='http://localhost:8000/passbook/',
            logo_text='Paw Planet'
        )

        device = Device.objects.create(
            device_library_identifier=self.device_id
        )

        PkPassRegistration.objects.create(
            pkpass=pkpass,
            device=device,
            push_token=self.push_token
        )

    def test_delete_registration(self):
        response = self.client.delete(
            self.register_delete_url_format.format(
                device_id=self.device_id,
                pass_type_id=self.pass_type_id,
                serial_number=self.serial_number
            ),
            **{
                'HTTP_AUTHORIZATION': 'ApplePass {auth_token}'.format(
                    auth_token=self.auth_token
                ),
            }
        )

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)


class PkPassRegistrationsEndpointTestCase(TestCase):

    def setUp(self):
        self.registrations_url_format = (
            '/v1/devices/{device_id}/' +
            'registrations/{pass_type_id}/'
        )
        self.client = Client()
        self.device_id = binascii.b2a_hex(os.urandom(15)).decode()
        self.pass_type_id = 'pass.com.example.coupon'
        self.serial_number = 'E5982H-I2'

        self.push_token = binascii.b2a_hex(os.urandom(15)).decode()

        self.auth_token = 'vxwxd7J8AlNNFPS8k0a0FfUFtq0ewzFdc'

        pkpass = PkPass.objects.create(
            description='20% off premium dog food',
            organization_name='Paw Planet',
            pass_type_identifier=self.pass_type_id,
            serial_number=self.serial_number,
            team_identifier='A1B2C3D4E5',
            pass_type=PkPass.COUPON_TYPE,
            authentication_token=self.auth_token,
            web_service_url='http://localhost:8000/passbook/',
            logo_text='Paw Planet'
        )

        device = Device.objects.create(
            device_library_identifier=self.device_id
        )

        PkPassRegistration.objects.create(
            pkpass=pkpass,
            device=device,
            push_token=self.push_token
        )

    def test_not_device_registrations(self):
        response = self.client.get(
            self.registrations_url_format.format(
                device_id='bad-device-id',
                pass_type_id=self.pass_type_id
            )
        )

        # Check that the response is 404 Not Found.
        self.assertEqual(response.status_code, 404)

    def test_no_registrations(self):
        response = self.client.get(
            self.registrations_url_format.format(
                device_id=self.device_id,
                pass_type_id='com.pass.badpasstype'  # For an empty response
            )
        )

        # Check that the response is 204 No Content.
        self.assertEqual(response.status_code, 204)

    def test_matching_passes(self):
        response = self.client.get(
            self.registrations_url_format.format(
                device_id=self.device_id,
                pass_type_id=self.pass_type_id
            )
        )

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

        self.assertEqual(
            json.loads(response.content.decode("utf-8"))['serialNumbers'],
            [self.serial_number]
        )

    def test_no_registrations_with_parameters(self):
        url_with_parameters = (
            self.registrations_url_format +
            '?passesUpdatedSince={tag}'
        )
        response = self.client.get(
            url_with_parameters.format(
                device_id=self.device_id,
                pass_type_id=self.pass_type_id,
                tag=(
                    timezone.now() + timezone.timedelta(1)
                ).isoformat()  # a day in future
            )
        )

        # Check that the response is 204 No Content.
        self.assertEqual(response.status_code, 204)

    def test_matching_passes_with_parameters(self):
        url_with_parameters = (
            self.registrations_url_format +
            '?passesUpdatedSince={tag}'
        )
        response = self.client.get(
            url_with_parameters.format(
                device_id=self.device_id,
                pass_type_id=self.pass_type_id,
                tag=(
                    timezone.now() - timezone.timedelta(1)
                ).isoformat()  # a day ago
            )
        )

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

        self.assertEqual(
            json.loads(response.content.decode("utf-8"))['serialNumbers'],
            [self.serial_number]
        )


class PkPassLatestVersionEndpointTestCase(TestCase):

    def setUp(self):
        self.latest_url_format = '/v1/passes/{pass_type_id}/{serial_number}/'
        self.client = Client()
        self.device_id = binascii.b2a_hex(os.urandom(15)).decode()
        self.pass_type_id = 'pass.com.example.coupon'
        self.serial_number = 'E5982H-I2'

        self.push_token = binascii.b2a_hex(os.urandom(15)).decode()

        self.auth_token = 'vxwxd7J8AlNNFPS8k0a0FfUFtq0ewzFdc'

        barcode = PkPassBarcode.objects.create(
            barcode_format=PkPassBarcode.QR,
            message='Test QR code.',
            message_encoding='iso-8859-1',
            alt_text='QR'
        )

        pkpass = PkPass.objects.create(
            description='20% off premium dog food',
            organization_name='Paw Planet',
            pass_type_identifier=self.pass_type_id,
            serial_number=self.serial_number,
            team_identifier='A1B2C3D4E5',
            pass_type=PkPass.BOARDING_TYPE,
            transit_type=PkPass.AIR_TRANSIT_TYPE,
            authentication_token=self.auth_token,
            web_service_url='http://localhost:8000/passbook/',
            logo_text='Paw Planet',
            barcode=barcode,
            expiration_date=(
                timezone.now() + timezone.timedelta(30)
            ).isoformat(),
            voided=True,
            background_color='#000000',
            foreground_color='#FFFFFF',
            label_color='#FFFFFF',
            suppress_strip_shine=False
        )

        PkPassField.objects.create(
            field_type=CurrencyPkPassField.TYPE_BACK,
            attributed_value=(
                "<a href='http://example.com/customers/123'>" +
                "Comprar ahora</a>"
            ),
            value='Comprar ahora',
            label='COMPRAR',
            key='action',
            pkpass=pkpass
        )

        DatePkPassField.objects.create(
            date_style=DatePkPassField.DATE_STYLE_MEDIUM,
            time_style=DatePkPassField.DATE_STYLE_FULL,
            field_type=DatePkPassField.TYPE_SECONDARY,
            ignores_time_zone=False,
            is_relative=False,
            value=timezone.now().isoformat(),
            label='EXPIRE',
            key='expire_date',
            text_alignment=DatePkPassField.ALIGN_NATURAL,
            change_message='Date changed to %@.',
            pkpass=pkpass
        )

        NumberPkPassField.objects.create(
            field_type=NumberPkPassField.TYPE_HEADER,
            number_style=NumberPkPassField.STYLE_PERCENT,
            value='20',
            label='PERCENT',
            key='percent',
            pkpass=pkpass
        )

        CurrencyPkPassField.objects.create(
            field_type=CurrencyPkPassField.TYPE_PRIMARY,
            currency_code='ARS',
            value='50',
            label='AMOUNT',
            key='amount',
            pkpass=pkpass
        )

        PkPassLocation.objects.create(
            latitude=123.6589,
            longitude=-64.0921451,
            altitude=50.021332,
            relevant_text='Test Locations',
            pkpass=pkpass
        )

        PkPassBeacon.objects.create(
            major=123,
            minor=43,
            proximity_uuid=binascii.b2a_hex(os.urandom(15)).decode(),
            relevant_text='Test Beacons',
            pkpass=pkpass
        )

        device = Device.objects.create(
            device_library_identifier=self.device_id
        )

        PkPassRegistration.objects.create(
            pkpass=pkpass,
            device=device,
            push_token=self.push_token
        )

    def test_not_authorized_latest_version(self):
        response = self.client.get(
            self.latest_url_format.format(
                pass_type_id=self.pass_type_id,
                serial_number=self.serial_number
            ),
            **{
                'HTTP_AUTHORIZATION': 'ApplePass {auth_token}'.format(
                    auth_token='BadToken'
                ),
            }
        )

        # Check that the response is 401 UNAUTHENTICATED.
        self.assertEqual(response.status_code, 401)

    def test_get_latest_version(self):
        response = self.client.get(
            self.latest_url_format.format(
                pass_type_id=self.pass_type_id,
                serial_number=self.serial_number
            ),
            **{
                'HTTP_AUTHORIZATION': 'ApplePass {auth_token}'.format(
                    auth_token=self.auth_token
                ),
            }
        )

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

        # TODO Check if the response is a valid pass.pkpass file


class PkPassLogEndpointCase(TestCase):

    def setUp(self):
        self.log_url = '/v1/log/'
        self.client = Client()

    def test_correct_log(self):
        response = self.client.post(self.log_url, {'logs': 'Random log.'})

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

    def test_bad_request_log(self):
        response = self.client.post(self.log_url, {'wrong_key': 'Random log.'})

        # Check that the response is 400 Bad Request.
        self.assertEqual(response.status_code, 400)
