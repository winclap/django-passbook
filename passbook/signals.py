# -*- coding: utf-8 -*-
from django.dispatch import Signal


pkpass_registered = Signal()
pkpass_unregistered = Signal()
